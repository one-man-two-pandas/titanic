from torch.utils.data import Dataset, DataLoader
import pandas as pd
import numpy as np


class PreparedTitanicDataset(Dataset):

    def __init__(self, data, target_labels):
        if target_labels is None:
            target_labels = np.zeros(len(data))
        self.num_data_points = len(data)
        self.data_size = len(data[0])
        parsed_target_labels = []
        for label in target_labels:
            parsed_target_labels.append(self.parse_single_label(label))
        self.labels = np.array(parsed_target_labels)
        self.data = data
        self.log = False

    def parse_single_label(self, value):
        if value == 1:
            return [1, 0]
        if value == 0:
            return [0, 1]
        raise ValueError

    def __len__(self):
        return self.num_data_points

    def __getitem__(self, idx):
        return self.data[idx], self.labels[idx]


def test_dataset():
    train_csv = pd.read_csv('../data/train.csv')
    dataset = PreparedTitanicDataset(train_csv)
    dataloader = DataLoader(dataset)

    for i, (inputs, targets) in enumerate(dataloader):
        print(str(targets))
        print(str(inputs))
