import pandas as pd
import hashlib

dataframe_keys = [
    'enc_title',
    'enc_last_name',
    'enc_ticket_prefix',
    'enc_ticket_id',
    'enc_ticket_id_num_digits',
    'enc_cabin',
]


def eval_idx(string, search):
    try:
        return string.index(search)
    except ValueError:
        return -1


def eval_space_index(name, dot_index):
    name = name[:dot_index]
    current_index = eval_idx(name, ' ')
    new_index = 0
    while new_index != -1:
        new_index = eval_idx(name, ' ')
        if new_index != -1:
            current_index = new_index
        name = name[current_index + 1:]
    return current_index


def hash_string(string):
    return int(hashlib.sha256(string.encode('utf-8')).hexdigest(), 16) % 10 ** 8


def encode_name_to_title(train_data):
    names = train_data['Name']
    res = []
    for idx, name in enumerate(names):
        dot_index = name.index('.')
        space_index = eval_space_index(name, dot_index)
        title = name[space_index + 1:dot_index]
        res.append(hash_string(title))
    train_data['enc_title'] = res


def encode_name_to_last_name(train_data):
    names = train_data['Name']
    res = []
    for idx, name in enumerate(names):
        comma_index = name.index(',')
        last_name = name[:comma_index]
        res.append(hash_string(last_name))
    train_data['enc_last_name'] = res


def encode_ticket(train_data):
    tickets = train_data['Ticket']
    ticket_prefix = []
    ticket_id = []
    ticket_id_num_digits = []

    for ticket in tickets:
        try:
            ticket_id.append(int(ticket))
            ticket_prefix.append(0)
            ticket_id_num_digits.append(len(str(ticket)))
        except ValueError:
            parsed_ticket = str(ticket).rsplit(maxsplit=1)
            if len(parsed_ticket) > 1:
                prefix = parsed_ticket[0].replace(' ', '').replace('.', '')
                ticket_prefix.append(hash_string(prefix))
                ticket_id.append(int(parsed_ticket[1]))
                ticket_id_num_digits.append(len(str(parsed_ticket[1])))
            else:
                ticket_id.append(0)
                ticket_prefix.append(hash_string(parsed_ticket[0]))
                ticket_id_num_digits.append(0)

    train_data['enc_ticket_prefix'] = ticket_prefix
    train_data['enc_ticket_id'] = ticket_id
    train_data['enc_ticket_id_num_digits'] = ticket_id_num_digits


def encode_cabin(train_data):
    cabins = train_data['Cabin']
    res = []
    for cabin in cabins:
        if str(cabin) == 'nan':
            ca = 0
        else:
            ca = hash_string(cabin.replace(' ', ''))
        res.append(ca)
    train_data['enc_cabin'] = res


def encode(train_data):
    encode_name_to_title(train_data)
    encode_name_to_last_name(train_data)
    encode_ticket(train_data)
    encode_cabin(train_data)


if __name__ == '__main__':
    train_data = pd.read_csv('../data/train.csv')
    encode_name_to_title(train_data)
    encode_name_to_last_name(train_data)
