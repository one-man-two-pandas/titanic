import numpy as np
import numpy_indexed as npi


class UnifiedDataEncoder:
    def __init__(self, start_index=2, fields=None):
        super().__init__()
        if fields is None:
            fields = ['pclass', 'title', 'sex', 'age', 'sib_sp', 'parch', 'fare']
        self.unified_data_dim = 0
        self.fields = fields
        self.start_index = start_index
        self.log = False

    def encode_prepared_field(self, grouped_field, field_values, result_array, size=-1):
        field_columns = self.get_field_value_distribution(grouped_field)
        for i, value in enumerate(field_values, start=0):
            mapped_field = np.zeros(len(field_columns))
            if size != -1:
                mapped_field = np.zeros(size)
            mapped_field[np.where(field_columns == value)[0][0]] = 1
            for currentValue in mapped_field:
                result_array[i].append(currentValue)

    def get_field_value_distribution(self, grouped_field):
        field_columns = grouped_field.unique
        field_counts = grouped_field.count
        if self.log:
            for i, name in enumerate(field_columns, start=0):
                print(str(name) + ' : ' + str(field_counts[i]))
        return field_columns

    def eval_idx(self, string, search):
        try:
            return string.index(search)
        except ValueError:
            return -1

    def eval_space_index(self, name, dot_index):
        name = name[:dot_index]
        current_index = self.eval_idx(name, ' ')
        new_index = 0
        while new_index != -1:
            new_index = self.eval_idx(name, ' ')
            if new_index != -1:
                current_index = new_index
            name = name[current_index + 1:]
        return current_index

    def parse_name_to_title(self, title_values):
        result = []
        for idx, name in enumerate(title_values):
            dot_index = name.index('.')
            space_index = self.eval_space_index(name, dot_index)
            title = name[space_index + 1:dot_index]
            result.append(title)
        return result

    def parse_name_to_last_name(self, name_field):
        result = []
        for idx, name in enumerate(name_field):
            comma_index = name.index(',')
            last_name = name[:comma_index]
            result.append(last_name)
        return result

    def parse_age_to_age_group(self, age_field):
        result = []
        float_age_field = np.nan_to_num(age_field.astype(np.float))
        for value in float_age_field:
            result.append(round(value / 10, 0))
        return result

    def parse_fare_to_fare_group(self, fare_field):
        result = []
        float_age_field = np.nan_to_num(fare_field.astype(np.float))
        for value in float_age_field:
            result.append(round(value / 10, 0))
        return result

    def encode(self, train_data):
        result_array = []
        for i in range(0, len(train_data)):
            result_array.append([])
        # Pclass
        if 'pclass' in self.fields:
            pclass_field = train_data[:, self.start_index]
            grouped_field = npi.group_by(pclass_field)
            self.encode_prepared_field(grouped_field, pclass_field, result_array, size=5)
        # Name parsings
        # - Title
        if 'title' in self.fields:
            title_field = train_data[:, self.start_index+1]
            parsed_title_field = self.parse_name_to_title(title_field)
            grouped_field = npi.group_by(parsed_title_field)
            self.encode_prepared_field(grouped_field, parsed_title_field, result_array, size=50)
        # Last name 667 unique values!
        if 'last_name' in self.fields:
            name_field = train_data[:, self.start_index+1]
            parsed_name_field = self.parse_name_to_last_name(name_field)
            grouped_field = npi.group_by(parsed_name_field)
            self.encode_prepared_field(grouped_field, parsed_name_field, result_array, size=700)
        # Sex
        if 'sex' in self.fields:
            sex_field = train_data[:, self.start_index+2]
            grouped_field = npi.group_by(sex_field)
            self.encode_prepared_field(grouped_field, sex_field, result_array, size=3)
        # Age
        if 'age' in self.fields:
            age_field = train_data[:, self.start_index+3]
            parsed_age_field = self.parse_age_to_age_group(age_field)
            grouped_field = npi.group_by(parsed_age_field)
            self.encode_prepared_field(grouped_field, parsed_age_field, result_array, size=15)
        # SibSp
        if 'sib_sp' in self.fields:
            sib_sp_field = train_data[:, self.start_index+4]
            grouped_field = npi.group_by(sib_sp_field)
            self.encode_prepared_field(grouped_field, sib_sp_field, result_array, size=15)
        # Parch
        if 'parch' in self.fields:
            parch_field = train_data[:, self.start_index+5]
            grouped_field = npi.group_by(parch_field)
            self.encode_prepared_field(grouped_field, parch_field, result_array, size=15)
        # Fare
        if 'fare' in self.fields:
            fare_field = train_data[:, self.start_index+7]
            parsed_fare_group = self.parse_fare_to_fare_group(fare_field)
            grouped_field = npi.group_by(parsed_fare_group)
            self.encode_prepared_field(grouped_field, parsed_fare_group, result_array, size=30)
        return np.array(result_array)
