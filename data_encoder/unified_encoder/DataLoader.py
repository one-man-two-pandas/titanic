import pandas as pd


class DataLoader:
    def __init__(self, csv_file):
        self.data = pd.read_csv(csv_file)

    def names_to_title(self, names):
        titles = []
        for idx, name in enumerate(names):
            name.find(',')
            name.find('.')
            titles.append(name[name.find(' ') + 1:name.find('.')])
        return titles

    def names_to_surname(self, names):
        surnames = []
        for idx, name in enumerate(names):
            surname = name[:name.find(',')]
            surnames.append(surname)
        return surnames

    @property
    def data_dict(self):
        return self.data.to_dict()

    @property
    def names(self):
        return list(self.data_dict['Name'].values())

    @property
    def surnames(self):
        return self.names_to_surname(self.names)

    @property
    def titles(self):
        return self.names_to_title(self.names)

    @property
    def pclass(self):
        return list(self.data_dict['Name'].values())

    @property
    def sex(self):
        return list(self.data_dict['Sex'].values())

    @property
    def age(self):
        return list(self.data_dict['Age'].values())

    @property
    def sib_sp(self):
        return list(self.data['SibSp'].values())

    @property
    def parch(self):
        return list(self.data['Parch'].values())

    @property
    def ticket(self):
        return list(self.data['Ticket'].values())

    @property
    def fare(self):
        return list(self.data['Fare'].values())

    @property
    def cabin(self):
        return list(self.data['Cabin'].values())

    @property
    def embarked(self):
        return list(self.data['Embarked'].values())
