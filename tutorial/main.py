from os import path
import os
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
import numpy as np
from sklearn.impute import SimpleImputer
import data_encoder


def check_data_dir():
    if not path.exists('../data'):
        raise IOError('Data dir does not exist! Download the titanic data set from kaggle.com.')


def evaluate_performance(prediction, ground_truth):
    correct = 0
    for pred, val in zip(prediction, ground_truth):
        if pred == val:
            correct = correct + 1
    return correct/len(prediction)


def create_out_dir():
    if not path.exists('../out'):
        os.makedirs('../out')


if __name__ == '__main__':
    check_data_dir()
    create_out_dir()
    train_data = pd.read_csv('../data/train.csv')
    test_data = pd.read_csv('../data/test.csv')
    # add computed values
    data_encoder.encode(train_data)
    data_encoder.encode(test_data)
    # random forrest model
    res_column = train_data['Survived']
    features = [
        'Pclass',
        'Sex',
        'Age',
        'SibSp',
        'Parch',
        'Fare',
        'Embarked',
        'enc_ticket_prefix',
        'enc_ticket_id',
        'enc_ticket_id_num_digits',
        'enc_title',
        'enc_last_name',
        # 'enc_cabin' # makes the result worse
    ]
    # fill null/nan values
    X = pd.get_dummies(train_data[features])
    X_test = pd.get_dummies(test_data[features])
    imp = SimpleImputer(missing_values=np.nan, strategy='mean')
    X = imp.fit_transform(X)
    X_test = imp.fit_transform(X_test)
    # model
    model = RandomForestClassifier(n_estimators=200, max_depth=7, random_state=1)
    model.fit(X, res_column)
    predictions = model.predict(X_test)

    # test performance on train dataset
    print('Performance on train dataset: ' + str(evaluate_performance(model.predict(X), res_column)))

    output = pd.DataFrame({'PassengerId': test_data.PassengerId, 'Survived': predictions})

    output.to_csv('out/my_submission.csv', index=False)
    print('Your submission was successfully saved!')