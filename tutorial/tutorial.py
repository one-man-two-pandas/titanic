from os import path
import os
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
import numpy as np
from sklearn.impute import SimpleImputer


def check_data_dir():
    if not path.exists('../data'):
        raise IOError('Data dir does not exist! Download the titanic data set from kaggle.com.')


def evaluate_performance(prediction, ground_truth):
    correct = 0
    for pred, val in zip(prediction, ground_truth):
        if pred == val:
            correct = correct + 1
    return correct/len(prediction)


def create_out_dir():
    if not path.exists('../out'):
        os.makedirs('../out')


if __name__ == '__main__':
    check_data_dir()
    create_out_dir()
    train_data = pd.read_csv('../data/train.csv')
    test_data = pd.read_csv('../data/test.csv')
    # print header of test data
    print(test_data.head())
    # example validation (all women survive all men die)
    women = train_data.loc[train_data.Sex == 'female']['Survived']
    rate_women = sum(women) / len(women)
    print(rate_women)
    # random forrest model
    y = train_data['Survived']
    features = [
        'Pclass',
        'Sex',
        'Age',
        'SibSp',
        'Parch',
        # 'Ticket',
        'Fare',
        # 'Cabin',
        'Embarked',
    ]
    X = pd.get_dummies(train_data[features])
    X_test = pd.get_dummies(test_data[features])
    # fill nan with 0
    # X = pd.DataFrame(X).fillna(0)
    # X_test = pd.DataFrame(X_test).fillna(0)
    # fill with interpolation
    imp = SimpleImputer(missing_values=np.nan, strategy='mean')
    X = imp.fit_transform(X)
    X_test = imp.fit_transform(X_test)

    model = RandomForestClassifier(n_estimators=100, max_depth=5, random_state=1)
    model.fit(X, y)
    predictions = model.predict(X_test)

    # test performance on train dataset
    print('Performance on train dataset: ' + str(evaluate_performance(model.predict(X), y)))

    output = pd.DataFrame({'PassengerId': test_data.PassengerId, 'Survived': predictions})

    output.to_csv('out/my_submission.csv', index=False)
    print('Your submission was successfully saved!')
