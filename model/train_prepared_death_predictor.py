import numpy as np
import torch
import pandas as pd
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm
from sklearn.preprocessing import OrdinalEncoder
import torch.onnx

from DeathPredictor import DeathPredictor
from dataset.PreparedTitanicDataset import PreparedTitanicDataset
from sklearn.model_selection import train_test_split

device = 'cuda' if torch.cuda.is_available() else 'cpu'

input_csv = pd.read_csv('../data/train.csv')
train_csv, validation_csv = train_test_split(input_csv, test_size=0.1, random_state=1)
# convert to numpy array
x_train = train_csv.values
x_validation = validation_csv.values
# remove survived column
x_train = np.delete(x_train, 1, 1)
x_validation = np.delete(x_validation, 1, 1)
# remove nan + convert to string
x_validation[x_validation.astype(str) == 'nan'] = 0
x_train[x_train.astype(str) == 'nan'] = 0
x_train = x_train.astype(str)
x_validation = x_validation.astype(str)

# encode
encoder = OrdinalEncoder(handle_unknown="use_encoded_value", unknown_value=-1)
encoder.fit(x_train)
train_enc = encoder.transform(x_train)
test_enc = encoder.transform(x_validation)

train_data = PreparedTitanicDataset(train_enc, np.array(train_csv['Survived']))
train_dataloader = DataLoader(train_data, batch_size=32)

valid_data = PreparedTitanicDataset(test_enc, np.array(validation_csv['Survived']))
valid_dataloader = DataLoader(valid_data, batch_size=32)

# neural net
death_predictor = DeathPredictor(train_data.data_size, device=device)
if death_predictor == 'cuda':
    death_predictor = torch.nn.DataParallel(death_predictor)
model_path = '../out/death_predictor_model_best'

# training parameters
lr = 1e-3
weight_decay = 1e-4
num_epochs = 250
train_criterion = torch.nn.MSELoss()
optimizer = torch.optim.Adam(death_predictor.parameters(), lr=lr, weight_decay=weight_decay)
scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=num_epochs)

# recorde training progress
tensorboard_writer = SummaryWriter()


def train_one_epoch(epoch):
    death_predictor.train()
    train_loss = 0

    for i, (inputs, targets) in enumerate(train_dataloader):
        inputs, targets = inputs.to(device), targets.to(device)
        # compute output
        output = death_predictor(inputs.float())
        loss = train_criterion(output, targets.float())
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        train_loss += loss.item()

    return train_loss


def test(epoch):
    death_predictor.eval()
    correct = 0
    total = 0
    for i, (inputs, targets) in enumerate(valid_dataloader):
        inputs, targets = inputs.to(device), targets.to(device)
        # compute output
        output = death_predictor(inputs.float())
        _, prediction = output.max(1)
        correct += prediction.eq(targets[:, 1]).sum().item()
        total += targets.size(0)
    return 100. * correct / total


def save_model():
    checkpoint = {'state_dict': death_predictor.state_dict()}
    torch.save({'state_dict': checkpoint['state_dict']}, model_path + '.pth')


def export_as_onnx(net, data_size, file_name):
    if device == 'cuda':
        x = torch.randn(data_size, requires_grad=True).cuda()
    elif device == 'cpu':
        x = torch.randn(data_size, requires_grad=True).cpu()
    torch.onnx.export(net, x, file_name, export_params=True)


def train():
    best_acc = -float('inf')
    with tqdm(total=num_epochs) as t:
        for epoch in range(0, num_epochs):
            train_loss = train_one_epoch(epoch)
            test_acc = test(epoch)
            scheduler.step()
            if test_acc > best_acc:
                best_acc = test_acc
                save_model()
            tensorboard_writer.add_scalar('train_loss', train_loss, epoch)
            tensorboard_writer.add_scalar('test accuracy', test_acc, epoch)
            t.set_postfix({
                'train_loss': train_loss,
                'test_acc': test_acc,
            })
            t.update(1)
    print('Saved model with best validation accuracy %d' % best_acc)


train()
export_as_onnx(death_predictor, [1, train_data.data_size], model_path + '.onnx')
