import pandas as pd
from torch.utils.data import DataLoader
import torch

from DeathPredictor import DeathPredictor
from dataset.TitanicDataset import TitanicDataset

# path to saved model
model_path = './out/death_predictor_model_best.pth'

# check if GPU available
device = 'cuda' if torch.cuda.is_available() else 'cpu'

# load dataset
test_csv = pd.read_csv('./data/test.csv')
split_index = len(test_csv)
test_data = TitanicDataset(test_csv, data_start_index=1)
test_dataloader = DataLoader(test_data, batch_size=1)

death_predictor = DeathPredictor(test_data.data_size, checkpoint_path=model_path, device=device)


def predict():
    death_predictor.eval()
    result = []
    for i, (inputs, targets) in enumerate(test_dataloader):
        inputs = inputs.to(device)
        # compute output
        output = death_predictor(inputs.float())
        _, prediction = output.max(1)
        value = 0 if prediction.item() == 1 else 1
        result.append(value)
    return result


death_list = predict()

output = pd.DataFrame({'PassengerId': pd.read_csv('./data/test.csv').PassengerId, 'Survived': death_list})

output.to_csv('./out/my_submission.csv', index=False)
print('Your submission was successfully saved!')
