import os

import torch
from torch import nn


class DeathPredictor(nn.Module):

    def __init__(self, data_size, hidden_size=150, n_layers=3,
                 checkpoint_path=None, device='cpu'):
        super(DeathPredictor, self).__init__()
        self.input_dim = data_size
        self.hidden_size = hidden_size
        self.n_layers = n_layers
        self.device = device
        self.checkpoint_path = checkpoint_path

        # build layers
        layers = []
        for i in range(self.n_layers):
            layers.append(nn.Sequential(
                nn.Linear(self.input_dim if i == 0 else self.hidden_size, self.hidden_size),
                nn.ReLU(inplace=True),
            ))
        layers.append(nn.Linear(self.hidden_size, 2, bias=True))
        self.layers = nn.Sequential(*layers)

        if checkpoint_path is not None and os.path.exists(checkpoint_path):
            checkpoint = torch.load(checkpoint_path, map_location='cpu')
            if 'state_dict' in checkpoint:
                checkpoint = checkpoint['state_dict']
            self.load_state_dict(checkpoint)
            print('Loaded checkpoint from %s' % checkpoint_path)

        self.layers = self.layers.to(self.device)

    def forward(self, x):
        out = self.layers(x)
        return out
